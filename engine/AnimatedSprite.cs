﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace engine
{
    public class AnimatedSprite:Sprite
    {
        List<int> Textures = new List<int>();
        List<int> Width = new List<int>();
        List<int> Height = new List<int>();
        List<BoundingBox> BBS = new List<BoundingBox>();
        public int fps=10;
        int count;
        public int frame = 0;
        int lastframe;
        public AnimatedSprite()
            : base()
        {
        }

        void Update()
        {
            if (fps == 0) return;
            int max = (int)(Game.current.r.RenderFrequency / fps);
            if (count++ >= max)
            {
                count = 0;
                frame++;
                if (frame >= lastframe) frame = 0;
            }
        }
        public void AddFrame(Stream s)
        {
            AddFrame(new Bitmap(s));
        }

        public void AddFrame(Bitmap bitmap)
        {
            int tex = GlUtil.loadTex(bitmap);
            Textures.Add(tex);
            BBS.Add(new BoundingBox(bitmap));
            Width.Add(bitmap.Width);
            Height.Add(bitmap.Height);
            lastframe++;
        }

        public void AddFrame(Sprite s) {
            Textures.Add(s.getTexture());
            BBS.Add(new BoundingBox(s));
            Width.Add(s.getWidth());
            Height.Add(s.getHeight());
            lastframe++;
        }
        public override void Draw(double x, double y, double Hscale, double Vscale)
        {
            Update();
            this.Tex = Textures[frame];
            this.bb = BBS[frame];
            base.Draw(x, y, Hscale, Vscale);
        }
        public override int getHeight()
        {
            return Height[frame];
        }
        public override int getWidth()
        {
            return Width[frame];
        }
        public override PointF getCenter(double x, double y)
        {
            return BBS[frame].getCenter(x, y);
        }
    }
}
