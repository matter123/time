using System;
using System.IO;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System.Runtime.InteropServices;

namespace engine
{
	public static class Audio
	{
		static AudioContext AC;
		public static void Init ()
		{
			try {
				AC = new AudioContext ();
				//AL.Enable(ALCapability.Invalid);
			} catch {
				Console.Error.WriteLine("ERROR: error creating Audio contex(create Game First)");
			}
		}
		public static SFX loadSFX (Stream stream, AFF format)
		{
			SFX sfx=null;
			if (format == AFF.WAV) {
				int buf;
				DecodeWav(stream,out buf);
				sfx=new SFX(buf);
			}
			return sfx;
		}		
		static void DecodeWav (Stream stream,out int buf)
		{
			int riff_chnk_size;
			BinaryReader br = new BinaryReader(stream);
			br.ReadInt32();//riff
			riff_chnk_size=
			br.ReadInt32();//file size?
			br.ReadInt32();//wave
			br.ReadBytes(4);//fmt 
			int fmt_chnk_size=br.ReadInt32();
			int afmt=br.ReadInt16();
			int channels=br.ReadInt16();
			int samplerate=br.ReadInt32();
			int byterate=br.ReadInt32();
			int blockalign=br.ReadInt16();
			int bitspersample=br.ReadInt16();
			br.ReadInt32();//data
			int datachunksize=br.ReadInt32();
			byte[] b=br.ReadBytes(datachunksize);
			GCHandle gch=GCHandle.Alloc(b,GCHandleType.Pinned);
			ALFormat af=ALFormat.Mono8;
			if(channels==1&&bitspersample==16)af=ALFormat.Mono16;
			if(channels==2&&bitspersample== 8)af=ALFormat.Stereo8;
			if(channels==2&&bitspersample==16)af=ALFormat.Stereo16;
			buf=AL.GenBuffer();
			AL.BufferData(buf,af,gch.AddrOfPinnedObject(),datachunksize,samplerate);
		}
	}
}

