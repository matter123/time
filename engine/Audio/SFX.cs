using System;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using OpenTK;

namespace engine
{
	public class SFX
	{
		int bid;
		int sid;
		public SFX (int bid)
		{
			this.bid=bid;
			this.sid=AL.GenSource();
			AL.Source(sid,ALSourcef.Gain,0.3f);
			AL.Source(sid,ALSourceb.Looping,false);
		}
		public void play() {
			AL.Source(sid,ALSourcei.Buffer,bid);
			AL.SourcePlay(sid);
		}
		public void playLoop(int times) {
			AL.DeleteSourceSource(sid);
			for(int i=0;i<times;i++) {
				AL.SourceQueueBuffer(sid,bid);
			AL.SourcePlay(sid);
		}
	}
}

