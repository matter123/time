﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace engine
{
    public class BoundingBox
    {
        public int l = int.MaxValue;
        public int r = 0;
        public int t = 0;
        public int b = int.MaxValue;
        public BoundingBox(Bitmap s)
        {
            bool seekB=false;
            l = s.Width;
            b = s.Height;
            Bitmap bb=s;
            for (int y = 0; y < s.Height; y++)
            {
                bool clear = true;
                for (int x = 0; x < s.Width; x++)
                {
                    if (bb.GetPixel(x, y).A != 0)
                    {
                        l = Math.Min(l, x);
                        clear = false;
                    }
                }
                for (int x = s.Width-1; x >= 0; x--)
                {
                    if (bb.GetPixel(x, y).A != 0)
                    {
                        r = Math.Max(r, x);
                        break;
                    }
                }
                if (clear)
                {
                    if (!seekB)
                        t = Math.Max(t, y);
                    else
                    {
                        b = Math.Min(b, y);
                    }
                }
                else
                {
                    seekB = true;
                }
            }
            //Console.WriteLine("t={0} b={1} l={2} r={3}", t, b, l, r);
        }

        public BoundingBox(Sprite s) : this(s.getBitmap()) { }
        public PointF getCenter(double x,double y)
        {
            int w = r - l;
            int h = b - t;
            double cx = x + l + w / 2;
            double cy = y + l + h / 2;
            return new PointF((float)cx, (float)cy);
        }

        public int getWidth()
        {
            return r - l;
        }

        public int getHeight()
        {
            return b - t;
        }
    }
}
