using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace ComputerState
{
	public static class GeneralState
	{
		public enum OS {
			WIN32,
			WIN64,
			LINUX32,
			LINUX64,
			MACOSX,
			UNKNOWN,
		}
		public enum KeyLocks {
			NUM_LOCK,
			CAPS_LOCK,
			SCROLL_LOCK,
		}
		static OS os;
		public static void Init() {
			os=OS.UNKNOWN;
			int platform=(int)Environment.OSVersion.Platform;
			if(platform==4||platform==128) {
				os=OS.LINUX32;
				if(Environment.Is64BitOperatingSystem)os=OS.LINUX64;
			}
			if(platform==6)os=OS.MACOSX;
			if(platform<2)os=OS.WIN32;
			if(platform==2) {
				os=OS.WIN32;
				if(Environment.Is64BitOperatingSystem)os=OS.WIN64;
			}
		}
		public static bool isLocked(KeyLocks kl) {
			if(os==OS.WIN32||os==OS.WIN64) {
				switch(kl) {
				case KeyLocks.CAPS_LOCK:return Control.IsKeyLocked(Keys.CapsLock);
				case KeyLocks.NUM_LOCK:return Control.IsKeyLocked(Keys.NumLock);
				case KeyLocks.SCROLL_LOCK:return Control.IsKeyLocked(Keys.Scroll);
				}
			}
			if(os==OS.LINUX32||os==OS.LINUX64) {
				Process xset=new Process();
				xset.StartInfo.UseShellExecute=false;
				xset.StartInfo.RedirectStandardOutput=true;
				xset.StartInfo.FileName="xset";
				xset.StartInfo.Arguments="-q";
				xset.Start();
				string xout=xset.StandardOutput.ReadToEnd();
				xset.WaitForExit();
				String[] output=xout.Split(Environment.NewLine.ToCharArray());
				foreach(string line in output) {
					if(line.Contains("00:")) {
						string line2=line.Trim();
						line2=line2.Replace("  "," ");
						//Console.WriteLine(line2);
						string[] parts=line2.Split(":".ToCharArray());
						//Console.WriteLine(parts.Length);
						string temp="";
						string[] off=new string[3];
						int count=0;
						foreach(string part in parts) {
							count++;
							if(count==3)off[0]=part.Substring(0,Math.Min(5,part.Length)).Trim();
							if(count==5)off[1]=part.Substring(0,Math.Min(5,part.Length)).Trim();
							if(count==7)off[2]=part.Substring(0,Math.Min(5,part.Length)).Trim();
						}
						int index=0;
						if(kl==KeyLocks.NUM_LOCK)index=1;
						if(kl==KeyLocks.SCROLL_LOCK)index=2;
						string nf=off[index];
						if(nf=="on")return true;
						return false;
					}
				}
			}
			return false;
		}
	}
}

