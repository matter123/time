using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Drawing;
using engine.Internal;

namespace engine
{
	public class Game
	{
		public int CHeight = 768;
		public int CWidth = 1024;
		public int X;
		public int Y;
		public RenderWindow r;
        internal List<char> realdown = new List<char>();
        internal List<VKEY> virtualdown = new List<VKEY>();
        public static Game current;
		public Game ()
		{
            current = this;
			r=new RenderWindow(this,1024,768,"QDE",false);
		}
		public int getWidth () {return CWidth;}
		public int getHeight () {return CHeight;}


		public delegate void RenderHandler();
		public event RenderHandler Render;

		public delegate void UpdateHandler();
		public event UpdateHandler Update;

        public delegate void KeyHandler();
        public event KeyHandler KeyDown;
        public event KeyHandler KeyUp;


		internal void doRender ()
		{
			if(Render!=null)
			Render(); //call delegate
		}

		internal void doUpdate ()
		{
			if(Update!=null)
			Update(); //call delegate
		}

        internal void doKeyDown(OpenTK.Input.Key k)
        {
            KeyHolder kh = KeyHolder.decode(k);
            if (kh.isVirtualKey())
            {
                if (!virtualdown.Contains(kh.getVKey())) virtualdown.Add(kh.getVKey());
            }
            else
            {
                if (!realdown.Contains(kh.getRealKey())) realdown.Add(kh.getRealKey());
            }
            if (KeyDown != null)
            KeyDown();
        }

        internal void doKeyUp(OpenTK.Input.Key k)
        {
            KeyHolder kh = KeyHolder.decode(k);
            if (kh.isVirtualKey())
            {
                virtualdown.Remove(kh.getVKey());
            }
            else
            {
                realdown.Remove(kh.getRealKey());
            }
            if (KeyUp != null)
            KeyUp();
        }

		public void Create() {
			r.Run(60);
		}

        public bool isKeyDown(char key)
        {
            key = undoShift(key);
            return realdown.Contains(key);
        }

        private char undoShift(char key)
        {
            if (Char.IsLetter(key)) return Char.ToLower(key);
            switch (key)
            {
                case '!': return '1';
                case '@': return '2';
                case '#': return '3';
                case '$': return '4';
                case '%': return '5';
                case '^': return '6';
                case '&': return '7';
                case '*': return '8';
                case '(': return '9';
                case ')': return '0';
                case '_': return '-';
                case '+': return '=';
                case '~': return '`';
                case '{': return '[';
                case '}': return ']';
                case '|': return '\\';
                case ':': return ';';
                case '\"': return '\'';
                case '<': return ',';
                case '>': return '.';
                case '?': return '/';
                default : return key;
            }
        }
        public bool isKeyDown(VKEY key)
        {
            if (key == VKEY.ANY) return virtualdown.Count + realdown.Count !=0;
            return virtualdown.Contains(key);
        }
	}
}

