﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace engine
{
    public class GameObject
    {
        protected readonly int Key;
        protected static List<GameObject> active = new List<GameObject>();
        public event engine.Game.UpdateHandler Update;
        double x;
        double y;
        Sprite s;
        private bool frozen;
        private static int nextKey;

        public Sprite Sprite
        {
            get
            {
                return s;
            }
            set
            {
                s = value;
            }
        }

        public void SetX(double X)
        {
            x = X;
        }

        public void SetY(double Y)
        {
            y = Y;
        }

        public double GetY()
        {
            return y;
        }

        public double GetX()
        {
            return x;
        }

        public GameObject(Sprite s)
        {
            this.Key = nextKey++;
            active.Add(this);
            this.Sprite = s;
            Game.current.Render += Render;
            Game.current.Update += Update_int;
        }

        void Update_int()
        {
            if (!frozen)
            if (Update != null) Update();
        }

        void Render()
        {
            s.Draw(x, y);
        }

        public void Freeze()
        {
            frozen = true;
        } 

        public void UnFreeze()
        {
            frozen = false;
        }

        protected bool isSelf(GameObject go)
        {
            return this.Key == go.Key;
        }
    }
}
