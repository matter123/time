using System;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace engine
{
	public static class GlUtil
	{
		public static void loadTex (Bitmap img, out int id)
		{
			if(img==null)Console.Write("OUCH");
			int tex = 0;
			GL.GenTextures (1, out tex);
			BitmapData data = img.LockBits (new Rectangle (0, 0, img.Width, img.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.BindTexture (TextureTarget.Texture2D, tex);
			GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                                      data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
			img.UnlockBits (data);
			bool nearest=GL.GetString(StringName.Vendor).Contains("ATI")?
				true:GL.GetString(StringName.Vendor).Contains("intel")?true:false;
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
			                 nearest?(int)TextureMinFilter.Nearest:(int)TextureMinFilter.Linear);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMagFilter,
			                 nearest?(int)TextureMagFilter.Nearest:(int)TextureMagFilter.Linear);
			id = tex;
		}
		public static void updateTex (Bitmap img, int tex)
		{
			BitmapData data = img.LockBits (new Rectangle (0, 0, img.Width, img.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			GL.BindTexture (TextureTarget.Texture2D, tex);
			GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                                      data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
			img.UnlockBits (data);
			bool nearest=GL.GetString(StringName.Vendor).Contains("ATI")?
				true:GL.GetString(StringName.Vendor).Contains("intel")?true:false;
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
			                 nearest?(int)TextureMinFilter.Nearest:(int)TextureMinFilter.Linear);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMagFilter,
			                 nearest?(int)TextureMagFilter.Nearest:(int)TextureMagFilter.Linear);
		}

		public static void getVBO (int i, out uint[] vBOId)
		{
			throw new NotImplementedException ("working on it sorry");
			vBOId = new uint[i];
			GL.GenBuffers (i, vBOId);
		}

		public static void createFBO (int width, int height, out uint fbo, out uint tex, out uint db)
		{
			GL.GenTextures (1, out tex);
			GL.BindTexture (TextureTarget.Texture2D, tex);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
			GL.TexImage2D (TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                                      width, height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, IntPtr.Zero);
 
			// test for GL Error here (might be unsupported format)
 
			GL.BindTexture (TextureTarget.Texture2D, 0); // prevent feedback, reading and writing to the same image is a bad idea
 
			// Create Depth Renderbuffer
			GL.Ext.GenRenderbuffers (1, out db);
			GL.Ext.BindRenderbuffer (RenderbufferTarget.RenderbufferExt, db);
			GL.Ext.RenderbufferStorage (RenderbufferTarget.RenderbufferExt, (RenderbufferStorage)All.DepthComponent32, width, height);
			GL.Ext.GenFramebuffers (1, out fbo);
			GL.Ext.BindFramebuffer (FramebufferTarget.FramebufferExt, fbo);
			GL.Ext.FramebufferTexture2D (FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, tex, 0);
			GL.Ext.FramebufferRenderbuffer (FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, RenderbufferTarget.RenderbufferExt, db);
		}

        public static int loadTex(Bitmap bitmap)
        {
            int tex;
            GlUtil.loadTex(bitmap, out tex);
            return tex;
        }
    }
}

