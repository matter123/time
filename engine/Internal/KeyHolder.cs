﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Input;

namespace engine.Internal
{
    class KeyHolder
    {
        private char ckey;
        private bool real;
        private VKEY vkey;
        public KeyHolder(char key)
        {
            ckey = key;
            real = true;
        }
        public KeyHolder(VKEY key)
        {
            vkey = key;
            real = false;
        }
        public char getRealKey()
        {
            if (real) return ckey;
            throw new InvalidOperationException();
        }
        public VKEY getVKey()
        {
            if (!real) return vkey;
            throw new InvalidOperationException();
        }
        public bool isVirtualKey()
        {
            return !real;
        }
        public static KeyHolder decode(Key k)
        {
            #region giant switch statment for all supported keys
            switch (k)
            {
                #region printable keys
                case Key.A: return new KeyHolder('a');
                case Key.B: return new KeyHolder('b');
                case Key.C: return new KeyHolder('c');
                case Key.D: return new KeyHolder('d');
                case Key.E: return new KeyHolder('e');
                case Key.F: return new KeyHolder('f');
                case Key.G: return new KeyHolder('g');
                case Key.H: return new KeyHolder('h');
                case Key.I: return new KeyHolder('i');
                case Key.J: return new KeyHolder('j');
                case Key.K: return new KeyHolder('k');
                case Key.L: return new KeyHolder('l');
                case Key.M: return new KeyHolder('m');
                case Key.N: return new KeyHolder('n');
                case Key.O: return new KeyHolder('o');
                case Key.P: return new KeyHolder('p');
                case Key.Q: return new KeyHolder('q');
                case Key.R: return new KeyHolder('r');
                case Key.S: return new KeyHolder('s');
                case Key.T: return new KeyHolder('t');
                case Key.U: return new KeyHolder('u');
                case Key.V: return new KeyHolder('v');
                case Key.W: return new KeyHolder('w');
                case Key.X: return new KeyHolder('x');
                case Key.Y: return new KeyHolder('y');
                case Key.Z: return new KeyHolder('z');
                case Key.Number0: return new KeyHolder('0');
                case Key.Number1: return new KeyHolder('1');
                case Key.Number2: return new KeyHolder('2');
                case Key.Number3: return new KeyHolder('3');
                case Key.Number4: return new KeyHolder('4');
                case Key.Number5: return new KeyHolder('5');
                case Key.Number6: return new KeyHolder('6');
                case Key.Number7: return new KeyHolder('7');
                case Key.Number8: return new KeyHolder('8');
                case Key.Number9: return new KeyHolder('9');
                case Key.Keypad0: return new KeyHolder('0');
                case Key.Keypad1: return new KeyHolder('1');
                case Key.Keypad2: return new KeyHolder('2');
                case Key.Keypad3: return new KeyHolder('3');
                case Key.Keypad4: return new KeyHolder('4');
                case Key.Keypad5: return new KeyHolder('5');
                case Key.Keypad6: return new KeyHolder('6');
                case Key.Keypad7: return new KeyHolder('7');
                case Key.Keypad8: return new KeyHolder('8');
                case Key.Keypad9: return new KeyHolder('9');
                case Key.Tilde: return new KeyHolder('`');
                case Key.Minus: return new KeyHolder('-');
                case Key.KeypadMinus: return new KeyHolder('-');
                case Key.Plus: return new KeyHolder('=');
                case Key.KeypadPlus: return new KeyHolder('=');
                case Key.BracketLeft: return new KeyHolder('[');
                case Key.BracketRight: return new KeyHolder(']');
                case Key.BackSlash: return new KeyHolder('\\');
                case Key.Semicolon: return new KeyHolder(';');
                case Key.Quote: return new KeyHolder('\'');
                case Key.Comma: return new KeyHolder(',');
                case Key.Period: return new KeyHolder('.');
                case Key.KeypadDecimal: return new KeyHolder('.');
                case Key.Slash: return new KeyHolder('/');
                case Key.Space: return new KeyHolder(' ');
                #endregion
                #region virtual keys
                case Key.F1: return new KeyHolder(VKEY.KEY_F1);
                case Key.F2: return new KeyHolder(VKEY.KEY_F2);
                case Key.F3: return new KeyHolder(VKEY.KEY_F3);
                case Key.F4: return new KeyHolder(VKEY.KEY_F4);
                case Key.F5: return new KeyHolder(VKEY.KEY_F5);
                case Key.F6: return new KeyHolder(VKEY.KEY_F6);
                case Key.F7: return new KeyHolder(VKEY.KEY_F7);
                case Key.F8: return new KeyHolder(VKEY.KEY_F8);
                case Key.F9: return new KeyHolder(VKEY.KEY_F9);
                case Key.F10: return new KeyHolder(VKEY.KEY_F10);
                case Key.F11: return new KeyHolder(VKEY.KEY_F11);
                case Key.F12: return new KeyHolder(VKEY.KEY_F12);
                case Key.F13: return new KeyHolder(VKEY.KEY_F13);
                case Key.F14: return new KeyHolder(VKEY.KEY_F14);
                case Key.F15: return new KeyHolder(VKEY.KEY_F15);
                case Key.F16: return new KeyHolder(VKEY.KEY_F16);
                case Key.F17: return new KeyHolder(VKEY.KEY_F17);
                case Key.F18: return new KeyHolder(VKEY.KEY_F18);
                case Key.F19: return new KeyHolder(VKEY.KEY_F19);
                case Key.F20: return new KeyHolder(VKEY.KEY_F20);
                case Key.F21: return new KeyHolder(VKEY.KEY_F21);
                case Key.F22: return new KeyHolder(VKEY.KEY_F22);
                case Key.F23: return new KeyHolder(VKEY.KEY_F23);
                case Key.F24: return new KeyHolder(VKEY.KEY_F24);
                case Key.Up: return new KeyHolder(VKEY.KEY_UP);
                case Key.Down: return new KeyHolder(VKEY.KEY_DOWN);
                case Key.Left: return new KeyHolder(VKEY.KEY_LEFT);
                case Key.Right: return new KeyHolder(VKEY.KEY_RIGHT);
                case Key.WinLeft: return new KeyHolder(VKEY.KEY_WIN_L);
                case Key.WinRight: return new KeyHolder(VKEY.KEY_WIN_R);
                case Key.AltLeft: return new KeyHolder(VKEY.KEY_WIN_L);
                case Key.AltRight: return new KeyHolder(VKEY.KEY_WIN_R);
                case Key.ShiftLeft: return new KeyHolder(VKEY.KEY_SHIFT);
                case Key.ShiftRight: return new KeyHolder(VKEY.KEY_SHIFT);
                case Key.CapsLock: return new KeyHolder(VKEY.KEY_CAPSLOCK);
                case Key.NumLock: return new KeyHolder(VKEY.KEY_NUMLOCK);
                case Key.ControlLeft: return new KeyHolder(VKEY.KEY_CTRL_L);
                case Key.ControlRight: return new KeyHolder(VKEY.KEY_CTRL_R);
                case Key.Escape: return new KeyHolder(VKEY.KEY_ESC);
                case Key.Enter: return new KeyHolder(VKEY.KEY_ENTER);
                case Key.KeypadEnter: return new KeyHolder(VKEY.KEY_ENTER);
                case Key.End: return new KeyHolder(VKEY.KEY_END);
                case Key.Home: return new KeyHolder(VKEY.KEY_HOME);
                case Key.PageUp: return new KeyHolder(VKEY.KEY_PG_UP);
                case Key.PageDown: return new KeyHolder(VKEY.KEY_PG_DOWN);
                case Key.PrintScreen: return new KeyHolder(VKEY.KEY_PRNT_SCRN);
                case Key.Insert: return new KeyHolder(VKEY.KEY_INSERT);
                case Key.Delete: return new KeyHolder(VKEY.KEY_DEL);
                #endregion
                default:
                    {
                        Console.WriteLine("unsupported key pressd"); return new KeyHolder(0);
                    }
            }
            #endregion
        }
    }
}
