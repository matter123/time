using System;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using OpenTK.Input;

namespace engine
{
	public class RenderWindow:GameWindow
	{
		Game game;
		public RenderWindow(Game g,int width,int height,string title,bool fullscreen):
				base(width,height,GraphicsMode.Default,title,
            fullscreen?GameWindowFlags.Fullscreen:GameWindowFlags.Default) {
			this.game=g;
            Keyboard.KeyDown += Keyboard_KeyDown;
            Keyboard.KeyUp += Keyboard_KeyUp;
            string version = GL.GetString(OpenTK.Graphics.OpenGL.StringName.Version);
            string vendor = GL.GetString(OpenTK.Graphics.OpenGL.StringName.Vendor);
            Console.WriteLine("OpenGL V{0}.{1} by {2}", version[0], version[2],vendor);
			Console.WriteLine("VSYNC is {0}",this.VSync==VSyncMode.On?"on":"off");
		}

        void Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            game.doKeyUp(e.Key);
        }

        void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            game.doKeyDown(e.Key);
        }
		protected override void OnLoad (EventArgs e)
		{
			base.OnLoad(e); //basic setup
	       	GL.ClearColor(Color4.Black);
	        GL.Disable(EnableCap.DepthTest);
	        GL.Enable(EnableCap.Texture2D);  
	        GL.Enable(EnableCap.Blend);
	        GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            //GL.Enable(EnableCap.PolygonSmooth);
            //GL.Enable(EnableCap.LineSmooth);
            //GL.Hint(HintTarget.LineSmoothHint, HintMode.Nicest);
            //GL.Enable(EnableCap.Multisample);
	        GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
	        GL.MatrixMode(MatrixMode.Projection);
	        GL.LoadIdentity();
	        GL.Ortho(0,1024,768,0,-1,1);
	        GL.Viewport(0,0,this.Width,this.Height);
            GL.MatrixMode(MatrixMode.Modelview);
		}
		protected override void OnUpdateFrame (FrameEventArgs e)
		{
			base.OnUpdateFrame (e);
			game.doUpdate();
		}
		protected override void OnRenderFrame (FrameEventArgs e)
		{
			base.OnRenderFrame (e);
			GL.Clear(ClearBufferMask.ColorBufferBit);
			game.doRender();
			this.SwapBuffers();
		}
		protected override void OnMove (EventArgs e)
		{
			game.X=this.X;
			game.Y=this.Y;
		}
		protected override void OnResize (EventArgs e)
		{
			base.OnResize (e);
			game.CWidth=this.Width;
			game.CHeight=this.Height;
			GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0,1024,768,0,-1,1);
            GL.Viewport(0,0,this.Width,this.Height);
            GL.MatrixMode(MatrixMode.Modelview);
		}
	}
}

 