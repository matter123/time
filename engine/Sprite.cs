using System;
using System.IO;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using System.Net;

namespace engine
{
	public class Sprite
	{
		protected Bitmap b;
		protected int Tex;
        public double rotation=0;
		//test.plaid.png
        public BoundingBox bb;
        public Sprite(Stream s) : this(new Bitmap(s)) { }
        public Sprite(Bitmap b)
        {
            this.b = b;
            GlUtil.loadTex(b, out Tex);
            bb = new BoundingBox(this);
        }

        public Sprite()
        {
        }
		public void Draw(double x,double y) {
			Draw (x,y,1,1);
		}
		public void Draw(double x,double y,double Scale) {
			Draw (x,y,Scale,Scale);
		}
		public virtual void Draw(double x,double y,double Hscale, double Vscale) {
            double width = getWidth() * Hscale;
            double height = getHeight() * Vscale;
            double cx = width / 2 + x;
            double cy = height / 2 + y;
            double t=(-height/2);
            double l=(-width/2);
            GL.Translate(cx, cy, 0);
            GL.Rotate(rotation,OpenTK.Vector3d.UnitZ);
			GL.BindTexture(TextureTarget.Texture2D,Tex);
            /*
            GL.Disable(EnableCap.Texture2D);
            GL.Begin(BeginMode.Quads);
            GL.Vertex2(l + bb.l, t + bb.t);
            GL.Vertex2(l + bb.l, t + bb.t + bb.b);
            GL.Vertex2(l + bb.l + bb.r, t + bb.t + bb.b);
            GL.Vertex2(l + bb.l + bb.r, t + bb.t);
            GL.End();
            GL.Enable(EnableCap.Texture2D);/**/

            GL.Begin(BeginMode.Quads);
            GL.TexCoord2(0,0);
            GL.Vertex2(l,t);
            GL.TexCoord2(0,1);
            GL.Vertex2(l,t+height);
            GL.TexCoord2(1,1);
            GL.Vertex2(l+width,t+height);
            GL.TexCoord2(1,0);
            GL.Vertex2(l+width,t);
            GL.End();/**/
            GL.Rotate(-rotation, OpenTK.Vector3d.UnitZ);
            GL.Translate(-cx, -cy, 0);
			GL.BindTexture(TextureTarget.Texture2D,0);
		}
        public Sprite Scale(double scale)
        {
            return Scale(scale, scale);
        }
        public virtual Sprite Scale(double HScale, double VScale)
        {
            Sprite s = new Sprite();
            s.b = new Bitmap(this.b, new Size((int)(this.b.Width * HScale), (int)(this.b.Height * VScale)));
            s.bb = new BoundingBox(s.b);
            GlUtil.loadTex(s.b, out s.Tex);
            return s;
        }
        public Sprite Rotate(double angle)
        {
            Sprite s = new Sprite();
            s.b = new Bitmap(this.b, new Size(this.b.Width, this.b.Height));
            s.bb = new BoundingBox(s.b);
            s.Tex = Tex;
            s.rotation += angle+rotation;
            return s;
        }
        public Sprite RotateTo(double angle)
        {
            return Rotate(angle - rotation);
        }

        public Sprite SubSprite(int x, int y, int width, int height)
        {
            Bitmap b2 = b.Clone(new Rectangle(x, y, width, height), b.PixelFormat);
            Sprite s = new Sprite();
            s.b = b2;
            s.bb = new BoundingBox(s.b);
            GlUtil.loadTex(s.b, out s.Tex);
            return s;
        }
        public static Sprite LoadFromInternet(string url) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            return new Sprite(((HttpWebResponse)request.GetResponse()).GetResponseStream());
        }

        public int getTexture()
        {
            return Tex;
        }
        public virtual int getWidth()
        {
            return b.Width;
        }
        public virtual int getHeight()
        {
            return b.Height;
        }

        public virtual PointF getCenter(double x, double y)
        {
            return bb.getCenter(x, y);
        }

        internal Bitmap getBitmap()
        {
            return b;
        }
    }
}

