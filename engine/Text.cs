﻿using System;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
namespace engine
{
    public class Text
    {
        private Font f;
        private string text;
        private int texId, w, h;
        private Brush b;
        public Text(string text, Font f, Brush b)
        {
            this.text = text;
            this.f = f;
            this.b = b;
            updateTex();
        }

        public Text(string text, Font f, Color c) : this(text, f, new SolidBrush(c)) { }
        #region Renderable implementation
        public void render(int x, int y, double wscale, double hscale)
        {
            float w2 = w * (float)wscale;
            float h2 = h * (float)hscale;
            GL.BindTexture(TextureTarget.Texture2D, texId);
            GL.Begin(BeginMode.Quads);
            GL.TexCoord2(0, 0);
            GL.Vertex2(x, y);
            GL.TexCoord2(0, 1);
            GL.Vertex2(x, y + h2);
            GL.TexCoord2(1, 1);
            GL.Vertex2(x + w2, y + h2);
            GL.TexCoord2(1, 0);
            GL.Vertex2(x + w2, y);
            GL.End();
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }
        #endregion

        public Font font
        {
            get
            {
                return this.f;
            }
            set
            {
                f = value;
                updateTex();
            }
        }
        public int Width
        {
            get
            {
                return w;
            }
        }
        public Brush brush
        {
            get
            {
                return this.b;
            }
            set
            {
                this.b = value;
                updateTex();
            }
        }
        public string message
        {
            get
            {
                return this.text;
            }
            set
            {
                text = value;
                updateTex();
            }
        }

        public void updateTex()
        {
            Graphics g = Graphics.FromHwnd(IntPtr.Zero);
            Size s = g.MeasureString(message, font).ToSize();
            w = s.Width;
            h = s.Height;
            Bitmap img = new Bitmap(w, h);
            g = Graphics.FromImage(img);
            g.Clear(Color.FromArgb(0));
            g.DrawString(message, font, b, 0, 0);
            g.Flush();
            g.Dispose();
            if (texId == 0) GlUtil.loadTex(img, out texId);
            else GlUtil.updateTex(img, texId);
            img.Dispose();
        }
    }
}


