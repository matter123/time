﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace engine
{
    public enum VKEY
    {
        KEY_F1,
        KEY_F2,
        KEY_F3,
        KEY_F4,
        KEY_F5,
        KEY_F6,
        KEY_F7,
        KEY_F8,
        KEY_F9,
        KEY_F10,
        KEY_F11,
        KEY_F12,
        KEY_F13,
        KEY_F14,
        KEY_F15,
        KEY_F16,
        KEY_F17,
        KEY_F18,
        KEY_F19,
        KEY_F20,
        KEY_F21,
        KEY_F22,
        KEY_F23,
        KEY_F24,

        KEY_UP,
        KEY_DOWN,
        KEY_LEFT,
        KEY_RIGHT,

        KEY_WIN,
        KEY_WIN_L,
        KEY_WIN_R,

        KEY_ALT,
        KEY_ALT_L,
        KEY_ALT_R,

        KEY_SHIFT,
        KEY_CAPSLOCK,
        KEY_NUMLOCK,

        KEY_CTRL,
        KEY_CTRL_L,
        KEY_CTRL_R,

        KEY_ESC,
        KEY_ENTER,
        KEY_END,
        KEY_HOME,
        KEY_PG_UP,
        KEY_PG_DOWN,
        KEY_PRNT_SCRN,
        KEY_INSERT,
        KEY_DEL,

        ANY,
    }
}
