using engine;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace test
{
	public class GAME
	{
		static Sprite s;
        static Text t;
		public static void Main() {
			Game g=new Game();
			Audio.Init();
           // s = new Sprite(Assembly.GetExecutingAssembly().GetManifestResourceStream("test.moon.png")).SubSprite(8, 5, 19, 35);
            s = new Sprite(Assembly.GetExecutingAssembly().GetManifestResourceStream("test.moon.png"));
            AnimatedSprite asp = new AnimatedSprite();
			SFX sfx=Audio.loadSFX(Assembly.GetExecutingAssembly().GetManifestResourceStream("test.rcs"),AFF.WAV);
            asp.AddFrame(s.SubSprite(8 , 5, 19, 37).Scale(3));
            asp.AddFrame(s.SubSprite(32, 5, 19, 37).Scale(3));
            asp.AddFrame(s.SubSprite(56, 5, 19, 37).Scale(3));
            Sprite floor=s.SubSprite(0, 49, 128, 15).Scale((3));
            t = new Text("Press space to start", new Font("comic sans ms", 22),
                new SolidBrush(Color.BlanchedAlmond));
            asp.fps = 0;
            LanderGO l=new LanderGO(asp);
            l.SetX(30);
            l.SetY(30);
            l.Freeze();
            addFloor(g,floor);

			g.Render+=delegate {
                t.render(g.getWidth()/2-t.Width/2, 10, 1, 1);
                int x=0;
                while (g.getWidth() - x+floor.getWidth() > floor.getWidth())
                {
                    floor.Draw(x, (g.getHeight() + 5) - floor.getHeight());
                    x += floor.getWidth() - 5;
                }

			};
            g.Update += delegate
            {
                if (g.isKeyDown(' ')) {
					t.message="Good Luck";
                    l.UnFreeze();
					sfx.play();
				}
            };

			g.Create();
		}
        private static void addFloor(Game g,Sprite floor)
        {
            int x = 0;
            while (g.getWidth() - x + floor.getWidth() > floor.getWidth())
            {
                GameObject go = new GameObject(floor);
                go.SetX(x);
                go.SetY((g.getHeight() + 5) - floor.getHeight());
                x += floor.getWidth() - 5;
            }
        }
	}
}