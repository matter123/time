﻿using engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test
{
	class LanderGO : GameObject
	{
		private double yspeed, xspeed;
		private AnimatedSprite asprite;

		public LanderGO (AnimatedSprite asp)
            : base(asp)
		{
			this.asprite = asp;
			this.Update += LanderGO_Update;
			asprite.frame = 2;
			yspeed = 0.55;
		}

		bool down = true;
		//rcs 87,19
		void LanderGO_Update ()
		{
			if (Game.current.isKeyDown (VKEY.KEY_UP)) {
				yspeed -= 0.1;
				asprite.frame = 0;
			} else {
				asprite.frame = 2;
				yspeed += 0.01;
			}
			if (Game.current.isKeyDown (VKEY.KEY_LEFT)) {
				xspeed -= 0.02;
			}
			if (Game.current.isKeyDown (VKEY.KEY_RIGHT)) {
				xspeed += 0.02;
			}
			if (Game.current.isKeyDown (VKEY.KEY_DOWN)) {
				yspeed += 0.02;
			}
			foreach (GameObject go in active) {
				//Console.WriteLine("hi");
				if (!isSelf (go)) {
					int dx = (int)Math.Abs (this.Sprite.getCenter (GetX (), GetY ()).X - go.Sprite.getCenter (go.GetX (), go.GetY ()).X);
					int dy = (int)Math.Abs (this.Sprite.getCenter (GetX (), GetY ()).Y - go.Sprite.getCenter (go.GetX (), go.GetY ()).Y);
					if (this.Sprite.bb.getWidth () / 2 + go.Sprite.bb.getWidth () / 2 > dx && this.Sprite.bb.getHeight () / 2 + go.Sprite.bb.getHeight () / 2 > dy) {
						if (asprite.frame == 2)
							yspeed = xspeed = 0;
						asprite.frame = 2;
						if (Game.current.isKeyDown (VKEY.KEY_UP)) {
							yspeed -= 0.1;
							asprite.frame = 0;
						}
						//suspected collision
					}
				}
			}
			SetY (GetY () + yspeed);
			SetX (GetX () + xspeed);
		}
	}
}
